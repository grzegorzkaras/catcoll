import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import Box from "@mui/material/Box";
import RestartAltOutlinedIcon from "@mui/icons-material/RestartAltOutlined";
import BlockOutlinedIcon from '@mui/icons-material/BlockOutlined';
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import ButtonGroup from "@mui/material/ButtonGroup";

import { DownloadStatus } from "@shared/enums";
import {
  clearDownloads,
  selectCurrentIndex,
  selectLinks,
  selectStatus,
  setIndex,
  setStatus,
} from "@shared/store/features/downloadsSlice";

const DownloadController = () => {
  const [progressMessage, setProgressMessage] = useState("");
  const [downloadStateMessage, setDownloadStateMessage] = useState("");

  const links = useSelector(selectLinks);
  const status = useSelector(selectStatus);
  const currentIndex = useSelector(selectCurrentIndex);

  const dispatch = useDispatch();

  useEffect(() => {
    if (status === DownloadStatus.READY) {
      setProgressMessage(`Prepared: ${links.length} files.`);
      setDownloadStateMessage("Start Downloads");
    }

    if (status === DownloadStatus.DOWNLOADING) {
      setProgressMessage(`Downloading: ${currentIndex} out of ${links.length} files...`);
      setDownloadStateMessage("Downloading...");
    }

    if (status === DownloadStatus.INTERRUPTED) {
      setProgressMessage(`Download interrupted at ${currentIndex} out of ${links.length} files!`);
    }
  }, [links.length, status, currentIndex]);

  const beginDownloads = () => {
    if (status === DownloadStatus.READY) {
      dispatch(setStatus(DownloadStatus.DOWNLOADING));
    }
  };

  const retryDownloads = () => {
    if (status === DownloadStatus.INTERRUPTED) {
      dispatch(setIndex(currentIndex - 1));
      dispatch(setStatus(DownloadStatus.DOWNLOADING));
    }
  };

  const continueDownloads = () => {
    if (status === DownloadStatus.INTERRUPTED) {
      dispatch(setStatus(DownloadStatus.DOWNLOADING));
    }
  };

  const restartDownloads = () => {
    dispatch(clearDownloads());
    dispatch(setStatus(DownloadStatus.READY));
  };

  const interruptDownloads = () => {
    dispatch(setStatus(DownloadStatus.INTERRUPTED));
  }

  return (
    <Box
      component="div"
      sx={{
        display: "flex",
        justifyContent: "space-evenly",
        alignItems: "center",
        flexDirection: "column",
        width: 400,
        height: 200,
        p: 2,
      }}
    >
      <Typography variant="h5">{chrome.runtime.getManifest().name}:</Typography>
      <ButtonGroup variant="contained">
        {(status === DownloadStatus.READY || status === DownloadStatus.DOWNLOADING) && (
          <Button size="large" onClick={() => beginDownloads()} disabled={status === DownloadStatus.DOWNLOADING}>
            {downloadStateMessage}
          </Button>
        )}
        {status === DownloadStatus.INTERRUPTED && (
          <>
            <Button size="large" onClick={() => retryDownloads()}>
              Retry!
            </Button>
            <Button size="large" onClick={() => continueDownloads()}>
              Skip
            </Button>
          </>
        )}
        <Button size="large" onClick={() => restartDownloads()}>
          <RestartAltOutlinedIcon />
        </Button>
        <Button size="large" onClick={() => interruptDownloads()}>
          <BlockOutlinedIcon />
        </Button>
      </ButtonGroup>

      <Typography variant="h6" align="center">
        {progressMessage}
      </Typography>
    </Box>
  );
};

export default DownloadController;
