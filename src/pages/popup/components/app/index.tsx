import ThemeContainer from "@shared/components/theme-container";
import DownloadController from "../download-controller";

const App = () => (
  <ThemeContainer>
    <DownloadController />
  </ThemeContainer>
);

export default App;
