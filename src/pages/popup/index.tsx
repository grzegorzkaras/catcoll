import React from "react";
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";

import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

import { instantiateStore } from "@shared/store";
import App from "./components/app";
import { errorCatcher } from "@shared/logger";

async function init() {
  const store = await instantiateStore();

  const appContainer = document.querySelector("#app-container");

  if (!appContainer) {
    throw new Error("Cannot find app container");
  }

  const root = createRoot(appContainer);

  root.render(
    <React.StrictMode>
      <Provider store={store}>
        <App />
      </Provider>
    </React.StrictMode>
  );
}

errorCatcher(init)();
