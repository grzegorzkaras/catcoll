import * as R from "ramda";

import { errorCatcher } from "@shared/logger";
import { HistoryLink } from "@shared/types";

chrome.runtime.onMessage.addListener(
  R.compose(
    R.always(true),
    errorCatcher(async (message, sender, sendResponse) => {
      if (message.checkVisits) {
        const links: Array<string> = message.checkVisits;

        const result = await Promise.all(
          links.map(async (url) => {
            const visits = await chrome.history.getVisits({ url });
            const historyLink: HistoryLink = { url, wasVisited: visits.length !== 0 };
            return historyLink;
          })
        );

        sendResponse(result);
        return;
      }

      sendResponse();
    })
  )
);

const historyRemoveLinkMenuItemId = "catcoll_remove_link";

chrome.runtime.onInstalled.addListener(() => {
  chrome.contextMenus.create({
    title: "Remove link from history",
    contexts: ["link"],
    id: historyRemoveLinkMenuItemId,
  });
});

chrome.contextMenus.onClicked.addListener((info) => {
  if (info.menuItemId === historyRemoveLinkMenuItemId && info.linkUrl) {
    chrome.history.deleteUrl({ url: info.linkUrl });
  }
});

export {};
