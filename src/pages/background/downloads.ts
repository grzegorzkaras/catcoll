import * as R from "ramda";

import { LinkElement } from "@shared/types";
import { DownloadStatus } from "@shared/enums";
import { addGlobalChangeListener, instantiateStore } from "@shared/store";
import { SelectorsListState, selectSelectorsList } from "@shared/store/features/selectorsListSlice";
import {
  selectLinks,
  selectCurrentIndex,
  clearDownloads,
  setIndex,
  setStatus,
  DownloadLink,
  selectStatus,
} from "@shared/store/features/downloadsSlice";
import { errorCatcher } from "@shared/logger";

const COMPLETE: chrome.downloads.DownloadState = "complete";
const IN_PROGRESS: chrome.downloads.DownloadState = "in_progress";
const INTERRUPTED: chrome.downloads.DownloadState = "interrupted";

const retrieveFileURL = async (link: DownloadLink, selectorsList: SelectorsListState) => {
  const sourceSelector = selectorsList.find(({ siteName }) => link.source.includes(siteName));
  const { traversalList } = sourceSelector || {};

  if (!traversalList || traversalList === "" || traversalList === "[]") {
    return link.url;
  }

  const { id: tabId } = await chrome.tabs.create({ url: link.url, active: false, index: 0 });

  if (!tabId) {
    throw new Error("Tab created for file URL retrieval has no ID.");
  }

  const traversalSelectors = Array<string>(JSON.parse(traversalList));

  for (const [i, traversalSelector] of traversalSelectors.entries()) {
    const injectionResults = await chrome.scripting.executeScript({
      target: { tabId },
      func: (traversalSelector) => {
        const element = document.querySelector<LinkElement>(traversalSelector);
        
        if (element) {
          return ("href" in element) ? element.href : ("currentSrc" in element) ? (element.currentSrc === "") ? element.src : element.currentSrc : null;
        }
      },
      args: [traversalSelector],
    });

    const newURL = injectionResults[0].result;

    if (!newURL) {
      throw new Error("File url retrieval process used CSS selector that doesn't point to element with an url.");
    }

    if (i === traversalSelectors.length - 1) {
      await chrome.tabs.remove(tabId);
      return newURL;
    } else {
      await chrome.tabs.update(tabId, { url: newURL });
    }
  }

  await chrome.tabs.remove(tabId);
  return link.url;
};

type DownloadNextFileArgs = {
  state?: chrome.downloads.StringDelta;
  error?: chrome.downloads.StringDelta;
};

const downloadNextFile = async ({ state, error }: DownloadNextFileArgs = { state: { current: COMPLETE } }) => {
  const store = await instantiateStore();
  const links = selectLinks(store.getState());
  const currentIndex = selectCurrentIndex(store.getState());
  const selectorsList = selectSelectorsList(store.getState());

  if (currentIndex >= links.length) {
    store.dispatch(setStatus(DownloadStatus.READY));
    store.dispatch(clearDownloads());
  } else if (state?.current === COMPLETE) {
    store.dispatch(setStatus(DownloadStatus.DOWNLOADING));

    const url = await retrieveFileURL(links[currentIndex], selectorsList);

    await chrome.history.addUrl({ url });
    await chrome.downloads.download({ url, saveAs: false });

    store.dispatch(setIndex(currentIndex + 1));
  } else if (state?.previous === IN_PROGRESS && state?.current === INTERRUPTED) {
    store.dispatch(setStatus(DownloadStatus.INTERRUPTED));
    throw new Error(`Download interrupted: ${error ? error.current : "unknown error"}`);
  }
};

addGlobalChangeListener(
  errorCatcher(async (store, oldState) => {
    const state = store.getState();

    if (
      oldState.downloads.status !== DownloadStatus.DOWNLOADING &&
      state.downloads.status === DownloadStatus.DOWNLOADING
    ) {
      await downloadNextFile();
    }
  })
);

chrome.downloads.onChanged.addListener(
  errorCatcher(async ({ id, state, error }) => {
    const [{ byExtensionId }] = await chrome.downloads.search({ id });

    if (byExtensionId === chrome.runtime.id) {
      await downloadNextFile({ state, error });
    }
  })
);

chrome.downloads.onDeterminingFilename.addListener(
  R.compose(
    R.always(true),
    errorCatcher(({ byExtensionId, filename }, suggest) => {
      if (byExtensionId === chrome.runtime.id) {
        const conflictAction: chrome.downloads.FilenameConflictAction = "prompt";
        suggest({ filename: `Catcoll/${filename}`, conflictAction });
      } else {
        suggest();
      }
    })
  )
);

chrome.runtime.onStartup.addListener(
  errorCatcher(async () => {
    const store = await instantiateStore();
    const status = selectStatus(store.getState());

    if (status === DownloadStatus.DOWNLOADING) {
      store.dispatch(setStatus(DownloadStatus.INTERRUPTED));
    }
  })
);

export {};
