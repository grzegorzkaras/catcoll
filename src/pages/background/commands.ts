import { switchSelectionMode } from "@manifest";
import { errorCatcher } from "@shared/logger";
import { instantiateStore } from "@shared/store";
import { toggle } from "@shared/store/features/selectionModeSlice";

chrome.commands.onCommand.addListener(
  errorCatcher(async (command) => {
    if (command === switchSelectionMode) {
      const store = await instantiateStore();
      store.dispatch(toggle());
    }
  })
);

export {};
