import { HighlightElement, HistoryLink, LinkElement } from "@shared/types";
import { instantiateStore } from "@shared/store";
import { selectSelectorsList } from "@shared/store/features/selectorsListSlice";
import { selectSelectionMode } from "@shared/store/features/selectionModeSlice";
import {
  addLink,
  addLinks,
  DownloadLink,
  removeLink,
  removeLinks,
  selectLinks,
  selectStatus,
} from "@shared/store/features/downloadsSlice";

import Overlay from "./Overlay";
import { DownloadStatus } from "@shared/enums";

(async () => {
  const store = await instantiateStore();

  const selectorsList = selectSelectorsList(store.getState());
  const siteSelector = selectorsList.find(({ siteName }) => document.URL.includes(siteName));
  const { elementsToExtract, elementsToHighlight } = siteSelector || {};

  const getElements = <T extends Element>(selector?: string) => [
    ...(selector ? document.querySelectorAll<T>(selector) : document.links),
  ];

  const extractLink = (element: LinkElement) => {
    if ("href" in element) {
      return element.href;
    }
    
    if ("currentSrc" in element) {
      if (!element.currentSrc || element.currentSrc === "") {
        if ("src" in element) {
          return element.src;
        }
      } else {
        return element.currentSrc;
      }
    }
  
    throw new Error("Element has no link");
  }

  const checkVisits = (linkElements: Array<LinkElement>) =>
    chrome.runtime.sendMessage<{ checkVisits: Array<string> }, Array<HistoryLink>>({
      checkVisits: linkElements.map(extractLink),
    });

  let linkElements: Array<LinkElement>;
  let highlightElements: Array<HighlightElement>;
  let historyLinks: Array<HistoryLink>;

  const refreshVariables = async () => {
    linkElements = getElements<LinkElement>(elementsToExtract);
    highlightElements = getElements<HighlightElement>(elementsToHighlight);
    historyLinks = await checkVisits(linkElements);
  };

  await refreshVariables();

  function captureAllClicks(e: MouseEvent) {
    const highlightElementClicked = e.composedPath().some((el) => highlightElements.includes(el as LinkElement));

    if (highlightElementClicked) {
      e.stopPropagation();
      e.preventDefault();
    }

    const state = store.getState();

    if (selectStatus(state) !== DownloadStatus.READY) {
      return;
    }

    const path = e.composedPath();
    const clickedElementIndex = path.reduce((largestIndex, currentElement) => {
      const elementIndex = highlightElements.indexOf(<HTMLElement>currentElement);
      return elementIndex > largestIndex ? elementIndex : largestIndex;
    }, -1);

    if (clickedElementIndex !== -1) {
      const link = historyLinks[clickedElementIndex];

      const isAdded = selectLinks(state)
        .map(({ url }) => url)
        .includes(link.url);

      if (isAdded) {
        store.dispatch(removeLink({ url: link.url, source: document.URL }));
      } else {
        store.dispatch(addLink({ url: link.url, source: document.URL }));
      }
    }
  }

  function captureContextMenu(e: MouseEvent) {
    e.stopPropagation();
    e.preventDefault();

    const state = store.getState();

    if (selectStatus(state) !== DownloadStatus.READY) {
      return;
    }

    const addedLinks = selectLinks(state).map(({ url }) => url);
    const notVisitedLinks = historyLinks.filter((link) => !link.wasVisited);

    const noPageLinkAdded = notVisitedLinks.every((link) => !addedLinks.includes(link.url));

    const relevantLinks = notVisitedLinks.map((link): DownloadLink => ({ url: link.url, source: document.URL }));

    if (noPageLinkAdded) {
      store.dispatch(addLinks(relevantLinks));
    } else {
      store.dispatch(removeLinks(relevantLinks));
    }
  }

  const main = async () => {
    if (document.visibilityState === "hidden") {
      return;
    }

    const state = store.getState();

    if (selectSelectionMode(state)) {
      if (siteSelector) {
        document.addEventListener("click", captureAllClicks, true);
        document.addEventListener("contextmenu", captureContextMenu, true);
      }

      await refreshVariables();

      const elements = highlightElements.map((element, i) => ({
        element,
        wasVisited: historyLinks[i].wasVisited,
        isAdded: selectLinks(state)
          .map(({ url }) => url)
          .includes(historyLinks[i].url),
      }));

      Overlay.on(elements);
    } else {
      if (siteSelector) {
        document.removeEventListener("click", captureAllClicks, true);
        document.removeEventListener("contextmenu", captureContextMenu, true);
      }

      Overlay.off();
    }
  };

  store.subscribe(main);
  document.addEventListener("visibilitychange", main, false);
  await main();
})();
