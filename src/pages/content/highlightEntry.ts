import { Cleanup } from "@shared/types";
import injectHighlightSVGFilter from "./injectHighlightSVGFilter";
import Stylizer from "./Stylizer";

export type HighlightEntry = {
  element: HTMLElement;
  wasVisited: boolean;
  isAdded: boolean;
};

const primaryColor = "#0b59ff";
const secondaryColor = "#ff7100";
const { markedFilterID, framedFilterID } = injectHighlightSVGFilter({ primaryColor, secondaryColor });

const highlightEntry = ({ element, wasVisited = false, isAdded = false }: HighlightEntry): Cleanup => {
  const elementStylizer = new Stylizer(element);

  if (element.innerText) {
    const size = isAdded ? 2 : 1;

    elementStylizer.set({
      filter: wasVisited ? "saturate(2) brightness(0.8)" : isAdded ? "brightness(1.4)" : "",
      color: secondaryColor,
      "text-shadow": `1px   1px ${size}px ${primaryColor},
                      1px  -1px ${size}px ${primaryColor},
                      -1px  1px ${size}px ${primaryColor},
                      -1px -1px ${size}px ${primaryColor},
                      0 0 3px #000000`,
    });
  } else {
    elementStylizer.set({
      "z-index": "2147483647",
      filter: wasVisited
        ? `url(#${markedFilterID}) url(#${framedFilterID}) drop-shadow(0 0 4px #000000) ${isAdded ? "saturate(2) brightness(0.2) grayscale(0.6)" : "grayscale(1)"}`
        : isAdded
          ? `url(#${markedFilterID}) url(#${framedFilterID}) drop-shadow(0 0 4px #000000)`
          : `url(#${framedFilterID}) drop-shadow(0 0 4px #000000)`,
    });
  }

  return () => {
    elementStylizer.restore();

    return null;
  };
};

export default highlightEntry;


/*
import { Cleanup } from "@shared/types";
import injectHighlightSVGFilter from "./injectHighlightSVGFilter";
import Stylizer from "./Stylizer";

export type HighlightEntry = {
  element: HTMLElement;
  wasVisited: boolean;
  isAdded: boolean;
};

const primaryColor = "#0b59ff";
const secondaryColor = "#ff7100";
const highlightFilterID = injectHighlightSVGFilter();

const highlightEntry = ({ element, wasVisited = false, isAdded = false }: HighlightEntry): Cleanup => {
  const elementStylizer = new Stylizer(element);

  if (element.innerText) {
    const size = isAdded ? 2 : 1;

    elementStylizer.set({
      filter: wasVisited ? "saturate(2) brightness(0.8)" : isAdded ? "brightness(1.4)" : "",
      color: secondaryColor,
      "text-shadow": `1px   1px ${size}px ${primaryColor},
                      1px  -1px ${size}px ${primaryColor},
                      -1px  1px ${size}px ${primaryColor},
                      -1px -1px ${size}px ${primaryColor},
                      0 0 3px #000000`,
    });
  } else {
    elementStylizer.set({
      filter: wasVisited
        ? `url(#${highlightFilterID}) ${isAdded ? "saturate(2) brightness(0.2) grayscale(0.6)" : "grayscale(1)"}`
        : isAdded
        ? `url(#${highlightFilterID})`
        : "",
      "box-shadow": `0 0 0 4px ${secondaryColor}, 0 0 0 5px ${primaryColor}, 0 0 12px 4px #000000`,
    });
  }

  return () => {
    elementStylizer.restore();

    return null;
  };
};

export default highlightEntry;

*/