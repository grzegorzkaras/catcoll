class Stylizer {
  previousStyle: string | null = null;
  element: HTMLElement;

  constructor(element: HTMLElement) {
    this.element = element;
  }

  set(style: { [key: string]: string }) {
    this.previousStyle = this.element.getAttribute("style");

    for (const [property, value] of Object.entries(style)) {
      this.element.style.setProperty(property, value, "important");
    }
  }

  restore() {
    if (this.previousStyle) {
      this.element.setAttribute("style", this.previousStyle);
    } else {
      this.element.removeAttribute("style");
    }

    this.previousStyle = null;
  }
}

export default Stylizer;
