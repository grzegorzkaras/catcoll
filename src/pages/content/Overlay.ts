import { Cleanup } from "@shared/types";
import highlightEntry, { HighlightEntry } from "./highlightEntry";

type OverlayElements = Array<HighlightEntry>;

const createOverlay = (elements: OverlayElements) => {
  const cleanups = elements.map((element) => highlightEntry(element));

  return () => {
    cleanups.forEach((remove) => remove && remove());

    return null;
  };
};

let removeOverlay: Cleanup = null;

const Overlay = {
  on: async (elements: OverlayElements) => {
    if (removeOverlay) {
      removeOverlay = removeOverlay();
    }

    removeOverlay = createOverlay(elements);
  },
  off: () => {
    if (removeOverlay) {
      removeOverlay = removeOverlay();
    }
  },
};

export default Overlay;
