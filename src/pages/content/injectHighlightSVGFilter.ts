export type highlightSVGFilterColors = {
  primaryColor: string;
  secondaryColor: string;
};

function injectHighlightSVGFilter({ primaryColor, secondaryColor }: highlightSVGFilterColors) {
  const markedFilterID = `highlight-${chrome.runtime.id}-marked`;
  const framedFilterID = `highlight-${chrome.runtime.id}-framed`;
  const icon = chrome.runtime.getURL("public/128x128.png");

  const template = document.createElement("template");

  template.innerHTML = `
    <svg xmlns="http://www.w3.org/2000/svg" width="0" height="0" style="position: absolute;">
      <filter id="${markedFilterID}" primitiveUnits="objectBoundingBox" x="0%" y="0%" width="100%" height="100%">
        <feImage x="20%" y="20%" width="60%" height="60%" href="${icon}"/>
        <feColorMatrix type="matrix" 
          values="1 0 0 0 0
                  0 1 0 0 0
                  0 0 1 0 0
                  0 0 0 0.5 0"/>
        <feComposite in2="SourceGraphic" operation="over"/>
      </filter>

      <filter id="${framedFilterID}" primitiveUnits="objectBoundingBox" x="0%" y="0%" width="100%" height="100%">
        <feFlood result="outerRect"          x="0%" y="0%" width="100%" height="100%" flood-color="${secondaryColor}"/>
        <feFlood result="innerRect"          x="2%" y="2%" width="96%" height="96%" flood-color="${primaryColor}"/>
        <feFlood result="cutOutMiddleMask"   x="4%" y="4%" width="92%" height="92%" flood-color="#000000"/>

        <feComposite operator="over" in="innerRect" in2="outerRect" result="firstLayer"/>
        <feComposite operator="out" in="firstLayer" in2="cutOutMiddleMask" result="secondLayer"/>
        <feComposite operator="over" in="secondLayer" in2="SourceGraphic"/>
      </filter>
    </svg>
  `;

  document.documentElement.appendChild(template.content);

  return { markedFilterID, framedFilterID };
}

export default injectHighlightSVGFilter;
