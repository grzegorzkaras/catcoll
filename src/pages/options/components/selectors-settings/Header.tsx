import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Toolbar from "@mui/material/Toolbar";

const Header = () => (
  <Box component="div">
    <Toolbar>
      <Typography variant="h6" component="div" color="primary.contrastText">
        CSS Selectors:
      </Typography>
    </Toolbar>
  </Box>
);

export default Header;
