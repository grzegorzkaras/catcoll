import React from "react";
import { useSelector, useDispatch } from "react-redux";
import * as R from "ramda";

import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import IconButton from "@mui/material/IconButton";
import Divider from "@mui/material/Divider";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";

import { Selectors } from "@shared/types";
import { selectSelectorsList, removeSelectors, updateSelectors } from "@shared/store/features/selectorsListSlice";
import validateInput from "./validateInput";

type EditedField = {
  index: number;
  keyName: keyof Selectors | "";
};

const Body = () => {
  const selectorsList = useSelector(selectSelectorsList);
  const dispatch = useDispatch();

  const [editedField, setEditedField] = React.useState<EditedField>({ index: -1, keyName: "" });

  const open = ({ index, keyName }: EditedField) => {
    setEditedField({ index, keyName });
  };

  const close = () => {
    setEditedField({ index: -1, keyName: "" });
  };

  const keyNameToReadable = (keyName: string): string => {
    const words = keyName.match(/[A-Za-z][a-z]*/g) || [];
    return words.map((word) => word.charAt(0).toUpperCase() + word.substring(1)).join(" ");
  };

  return (
    <Box
      component="div"
      sx={{
        display: "flex",
        justifyContent: "center",
        minHeight: "100%",
        maxHeight: "100%",
        overflow: "auto",
        backgroundColor: "background.default",
      }}
    >
      <Stack spacing={1} sx={{ m: 2, height: "100%" }} divider={<Divider flexItem />}>
        {selectorsList.map(
          ({ siteName, elementsToExtract, elementsToHighlight, traversalList }: Selectors, index: number) => (
            <Paper
              key={index}
              component="div"
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                gap: 2,
                p: 2,
              }}
            >
              {R.toPairs({ siteName, elementsToExtract, elementsToHighlight, traversalList }).map(
                ([keyName, value], i) => {
                  const errorMessage = validateInput(keyName, value);

                  return (
                    <TextField
                      key={i}
                      label={`${keyNameToReadable(keyName)}:`}
                      onDoubleClick={() => open({ index, keyName })}
                      onChange={({ target: { value } }) => dispatch(updateSelectors({ index, keyName, value }))}
                      value={value}
                      error={errorMessage !== ""}
                      helperText={errorMessage}
                    />
                  );
                }
              )}
              <IconButton onClick={() => dispatch(removeSelectors({ index }))}>
                <DeleteOutlinedIcon />
              </IconButton>
            </Paper>
          )
        )}
      </Stack>
      <Dialog open={editedField.index >= 0} onClose={() => close()} fullWidth maxWidth="md">
        <DialogTitle>{`Edit ${keyNameToReadable(editedField.keyName)}:`}</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            multiline
            fullWidth
            minRows={8}
            maxRows={16}
            margin="dense"
            label={`${keyNameToReadable(editedField.keyName)}:`}
            onChange={({ target: { value } }) =>
              editedField.keyName !== "" &&
              dispatch(updateSelectors({ index: editedField.index, keyName: editedField.keyName, value }))
            }
            value={editedField.keyName !== "" ? selectorsList[editedField.index][editedField.keyName] : ""}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => close()}>Finish</Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};

export default Body;
