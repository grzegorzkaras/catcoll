import { useDispatch } from "react-redux";

import Box from "@mui/material/Box";
import Fab from "@mui/material/Fab";
import Toolbar from "@mui/material/Toolbar";
import AddIcon from "@mui/icons-material/Add";

import { createSelectors } from "@shared/store/features/selectorsListSlice";

const Footer = () => {
  const dispatch = useDispatch();

  return (
    <Box component="div">
      <Toolbar>
        <Fab
          color="primary"
          sx={{
            position: "absolute",
            zIndex: 1,
            top: -30,
            left: 0,
            right: 0,
            margin: "0 auto",
          }}
          onClick={() => dispatch(createSelectors())}
        >
          <AddIcon />
        </Fab>
      </Toolbar>
    </Box>
  );
};

export default Footer;
