import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";

import Header from "./Header";
import Body from "./Body";
import Footer from "./Footer";

const SelectorsSettings = () => (
  <Box component="div" sx={{ width: "100%", height: "100%", p: 7 }}>
    <Paper
      elevation={6}
      sx={{
        display: "flex",
        flexDirection: "column",
        borderRadius: 2,
        backgroundColor: "primary.dark",
        width: "100%",
        height: "100%",
      }}
    >
      <Header />
      <Box component="div" sx={{ flexGrow: 1, overflow: "hidden" }}>
        <Body />
      </Box>
      <Footer />
    </Paper>
  </Box>
);

export default SelectorsSettings;
