import { Selectors } from "@shared/types";

const validateURLSelector = (value: string): string => {
  try {
    new URL(value.startsWith("http") ? value : `http://${value}`);
    return "";
  } catch (exception) {
    return "Invalid URL";
  }
};

const isv = new CSSStyleSheet();

const validateCSSSelector = (value: string): string => {
  try {
    const body = ` { --body: "0"; }`;
    isv.insertRule(value + body);
    const selectorIsValid = isv.cssRules[0].cssText.endsWith(body);
    isv.deleteRule(0);

    if (selectorIsValid) {
      return "";
    } else {
      return "Invalid CSS selector";
    }
  } catch (exception) {
    return "Invalid CSS selector";
  }
};

const validateArrayOfCSSSelectors = (value: string): string => {
  try {
    const cssSelectorsArray = JSON.parse(value === "" ? "[]" : value);

    for (const selector of cssSelectorsArray) {
      const selectorValidation = validateCSSSelector(selector);

      if (selectorValidation !== "") {
        return selectorValidation;
      }
    }

    return "";
  } catch (exception) {
    return "Invalid array";
  }
};

const validateInput = (keyName: keyof Selectors, value: string): string => {
  switch (keyName) {
    case "siteName":
      return validateURLSelector(value);
    case "elementsToExtract":
      return validateCSSSelector(value);
    case "elementsToHighlight":
      return validateCSSSelector(value);
    case "traversalList":
      return validateArrayOfCSSSelectors(value);
  }
};

export default validateInput;
