import ThemeContainer from "@shared/components/theme-container";
import SelectorsSettings from "../selectors-settings";

const App = () => (
  <ThemeContainer>
    <SelectorsSettings />
  </ThemeContainer>
);

export default App;
