import React from "react";
import { useDispatch, useSelector } from "react-redux";

import ThemeProvider from "@mui/material/styles/ThemeProvider";
import createTheme from "@mui/material/styles/createTheme";
import CssBaseline from "@mui/material/CssBaseline";
import GlobalStyles from "@mui/material/GlobalStyles";
import Fab from "@mui/material/Fab";
import Box from "@mui/material/Box";
import Brightness7Icon from "@mui/icons-material/Brightness7";
import Brightness4Icon from "@mui/icons-material/Brightness4";

import { selectTheme, toggle } from "@shared/store/features/themeSlice";

type ThemeContainerProps = {
  children?: React.ReactNode;
};

const ThemeContainer = ({ children }: ThemeContainerProps) => {
  const theme = useSelector(selectTheme);
  const dispatch = useDispatch();

  const newTheme = createTheme({
    palette: {
      mode: theme,
      background: {
        default: theme === "light" ? "#E7EBF0" : "#303030",
        paper: theme === "light" ? "#ffffff" : "#343434",
      },
    },
  });

  return (
    <ThemeProvider theme={newTheme}>
      <CssBaseline />
      <GlobalStyles
        styles={{
          body: {
            width: "100%",
            height: "100vh",
            background: "palette.background.default",
          },
        }}
      />
      <Box component="div" sx={{ width: "100%", height: "100vh" }}>
        <Fab
          color="primary"
          size="small"
          sx={{
            position: "fixed",
            top: 10,
            right: 10,
          }}
          onClick={() => dispatch(toggle())}
        >
          {theme === "dark" ? <Brightness7Icon /> : <Brightness4Icon />}
        </Fab>
        {children}
      </Box>
    </ThemeProvider>
  );
};

export default ThemeContainer;
