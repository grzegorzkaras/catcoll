export enum DownloadStatus {
  READY,
  DOWNLOADING,
  INTERRUPTED,
}
