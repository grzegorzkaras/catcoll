export type Selectors = {
  siteName: string;
  elementsToExtract: string;
  elementsToHighlight: string;
  traversalList: string;
};

export type Cleanup = null | (() => null);

export type HistoryLink = {
  url: string;
  wasVisited: boolean;
};

export type ImageElement = HTMLImageElement;

export type VideoElement = HTMLVideoElement | HTMLMediaElement;

export type LinkElement = HTMLAnchorElement | HTMLAreaElement | VideoElement | ImageElement;

export type HighlightElement = HTMLElement;
