type LogData = string;

const log = (message: LogData, title = "Download All Tabs") => {
  console.log(message);

  chrome.notifications.create({
    type: "basic",
    iconUrl: chrome.runtime.getURL("public/128x128.png"),
    title: `${title}:`,
    message: message,
  });
};

const error = (error: LogData) => {
  console.error(error);

  chrome.notifications.create({
    type: "basic",
    iconUrl: chrome.runtime.getURL("public/128x128.png"),
    title: "Download All Tabs - Error:",
    message: error,
  });
};

export const errorCatcher =
  <T extends Array<unknown>, U>(fn: (...args: T) => U | void) =>
  async (...args: T): Promise<Awaited<U> | void> => {
    try {
      return await fn(...args);
    } catch (exception) {
      if (typeof exception === "string") {
        error(exception);
      } else if (exception instanceof Error) {
        error(exception.message);
      } else {
        error("Unknown error");
      }
    }
  };

const Logger = { log, error };

export default Logger;
