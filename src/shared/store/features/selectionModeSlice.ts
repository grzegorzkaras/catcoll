import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { RootState } from "..";

const initialState = false;

export const selectionModeSlice = createSlice({
  name: "selectionMode",
  initialState,
  reducers: {
    toggle: (state) => !state,
    set: (_, action: PayloadAction<boolean>) => action.payload,
  },
});

export const selectSelectionMode = (state: RootState) => state.selectionMode;

export const { toggle, set } = selectionModeSlice.actions;

export default selectionModeSlice.reducer;
