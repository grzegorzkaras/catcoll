import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { PaletteMode } from "@mui/material";

import { RootState } from "..";

const initialState = <PaletteMode>"dark";

export const themeSlice = createSlice({
  name: "theme",
  initialState,
  reducers: {
    toggle: (state) => (state === "dark" ? "light" : "dark"),
    set: (_, action: PayloadAction<PaletteMode>) => action.payload,
  },
});

export const selectTheme = (state: RootState) => state.theme;

export const { toggle, set } = themeSlice.actions;

export default themeSlice.reducer;
