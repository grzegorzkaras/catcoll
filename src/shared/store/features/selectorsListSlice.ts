import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import * as R from "ramda";

import { Selectors } from "@shared/types";
import { RootState } from "..";

export type SelectorsListState = Array<Selectors>;

const initialState: SelectorsListState = [];

export const selectorsListSlice = createSlice({
  name: "selectorsList",
  initialState,
  reducers: {
    createSelectors: (state) =>
      R.append({ siteName: "", elementsToExtract: "", elementsToHighlight: "", traversalList: "" }, state),
    removeSelectors: (state, action: PayloadAction<{ index: number }>) => R.remove(action.payload.index, 1, state),
    updateSelectors: (state, action: PayloadAction<{ index: number; keyName: keyof Selectors; value: string }>) =>
      R.update(
        action.payload.index,
        { ...state[action.payload.index], [action.payload.keyName]: action.payload.value },
        state
      ),
  },
});

export const selectSelectorsList = (state: RootState) => state.selectorsList;

export const { createSelectors, removeSelectors, updateSelectors } = selectorsListSlice.actions;

export default selectorsListSlice.reducer;
