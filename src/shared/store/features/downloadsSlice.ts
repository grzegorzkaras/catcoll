import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import * as R from "ramda";

import { DownloadStatus } from "@shared/enums";
import { RootState } from "..";

export type DownloadLink = {
  url: string;
  source: string;
};

export type DownloadsState = {
  links: Array<DownloadLink>;
  status: DownloadStatus;
  currentIndex: number;
};

const initialState: DownloadsState = {
  links: [],
  status: DownloadStatus.READY,
  currentIndex: 0,
};

export const downloadsSlice = createSlice({
  name: "downloads",
  initialState,
  reducers: {
    clearDownloads: (state) => ({ ...initialState, status: state.status }),
    addLink: (state, action: PayloadAction<DownloadLink>) =>
      R.set(R.lensProp("links"), [...state.links, action.payload], state),
    addLinks: (state, action: PayloadAction<Array<DownloadLink>>) =>
      R.set(R.lensProp("links"), [...state.links, ...action.payload], state),
    removeLink: (state, action: PayloadAction<DownloadLink>) =>
      R.set(
        R.lensProp("links"),
        state.links.filter((link) => link.url !== action.payload.url),
        state
      ),
    removeLinks: (state, action: PayloadAction<Array<DownloadLink>>) =>
      R.set(
        R.lensProp("links"),
        state.links.filter((link) => !action.payload.map(({ url }) => url).includes(link.url)),
        state
      ),
    setStatus: (state, action: PayloadAction<DownloadStatus>) => R.set(R.lensProp("status"), action.payload, state),
    setIndex: (state, action: PayloadAction<number>) => R.set(R.lensProp("currentIndex"), action.payload, state),
  },
});

export const selectLinks = (state: RootState) => state.downloads.links;

export const selectStatus = (state: RootState) => state.downloads.status;

export const selectCurrentIndex = (state: RootState) => state.downloads.currentIndex;

export const { clearDownloads, addLink, addLinks, removeLink, removeLinks, setStatus, setIndex } =
  downloadsSlice.actions;

export default downloadsSlice.reducer;
