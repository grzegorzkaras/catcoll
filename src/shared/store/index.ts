import * as R from "ramda";
import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { ChangeListener, setupReduxed } from "reduxed-chrome-storage";

import selectorsList from "./features/selectorsListSlice";
import theme from "./features/themeSlice";
import downloads from "./features/downloadsSlice";
import selectionMode from "./features/selectionModeSlice";
import Logger from "@shared/logger";

const rootReducer = combineReducers({
  selectorsList,
  theme,
  downloads,
  selectionMode,
});

export type RootState = ReturnType<typeof rootReducer>;

const onLocalChangeListeners: Array<ChangeListener> = [];
const onGlobalChangeListeners: Array<ChangeListener> = [];

export const addGlobalChangeListener = (listener: ChangeListener) => onGlobalChangeListeners.push(listener);
export const addLocalChangeListener = (listener: ChangeListener) => onGlobalChangeListeners.push(listener);

export const instantiateStore = setupReduxed(
  (preloadedState?: RootState) => configureStore({ reducer: rootReducer, preloadedState }),
  { storageKey: "store" },
  {
    onError: (message, exceeded) => Logger.error(`Redux store error: ${message}. Exceeded: ${exceeded}.`),
    onLocalChange: (store, oldState) => onLocalChangeListeners.forEach((listener) => listener(store, oldState)),
    onGlobalChange: (store, oldState) => onGlobalChangeListeners.forEach((listener) => listener(store, oldState)),
  }
);
