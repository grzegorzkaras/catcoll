export enum State {
  READY,
  DOWNLOADING,
  INTERRUPTED,
}
