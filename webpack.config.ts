import path from "path";
import fs from "fs";
import CopyWebpackPlugin from "copy-webpack-plugin";
import HtmlWebpackPlugin from "html-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import { Compiler } from "webpack";

import manifest from "./manifest";

type CreateFilePluginOptions = {
  filePath: string;
  content: string;
};

class CreateFilePlugin {
  options: CreateFilePluginOptions;

  constructor(options: CreateFilePluginOptions) {
    this.options = options;
  }

  apply(compiler: Compiler) {
    compiler.hooks.done.tap("CreateFilePlugin", () => {
      fs.writeFileSync(this.options.filePath, this.options.content);
    });
  }
}

const root = path.resolve(__dirname, "src");
const pagesDir = path.resolve(root, "pages");
const sharedDir = path.resolve(root, "shared");
const outDir = path.resolve(__dirname, "dist");
const manifestPath = path.resolve(__dirname, "manifest.ts");

const fileExtensions = ["jpg", "jpeg", "png", "gif", "eot", "otf", "svg", "ttf", "woff", "woff2"];

module.exports = {
  mode: "production",
  optimization: {
    minimize: false,
  },
  entry: {
    content: path.resolve(pagesDir, "content", "index.ts"),
    background: path.resolve(pagesDir, "background", "index.ts"),
    popup: path.resolve(pagesDir, "popup", "index.tsx"),
    options: path.resolve(pagesDir, "options", "index.tsx"),
  },
  output: {
    path: outDir,
    filename: "scripts/[name].bundle.js",
    clean: true,
  },
  resolve: {
    extensions: [".js", ".ts", ".tsx", ".html", ".css", ".json"],
    alias: {
      "@pages": pagesDir,
      "@shared": sharedDir,
      "@manifest": manifestPath,
      "@src": root,
    },
  },
  module: {
    rules: [
      {
        test: /\.(tsx|ts)?$/,
        use: ["ts-loader"],
      },
      {
        test: new RegExp(".(" + fileExtensions.join("|") + ")$"),
        type: "asset/resource",
        generator: {
          filename: "static/[hash][ext][query]",
        },
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "styles/[name].css",
    }),
    new CopyWebpackPlugin({
      patterns: [{ from: "./public", to: "./public" }],
    }),
    new CreateFilePlugin({
      filePath: path.join(outDir, "manifest.json"),
      content: JSON.stringify(manifest, null, "  "),
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(pagesDir, "popup", "index.html"),
      filename: "popup.html",
      scriptLoading: "module",
      chunks: ["popup"],
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(pagesDir, "options", "index.html"),
      filename: "options.html",
      scriptLoading: "module",
      chunks: ["options"],
    }),
  ],
};
