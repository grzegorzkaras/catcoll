import packageJson from "./package.json";

export const switchSelectionMode = "switch_selection_mode";

const [namesFirstLetter, ...namesOtherLetters] = packageJson.name;

const manifest: chrome.runtime.ManifestV3 = {
  manifest_version: 3,
  name: namesFirstLetter.toUpperCase() + namesOtherLetters.join(""),
  version: packageJson.version,
  description: packageJson.description,
  options_page: "options.html",
  background: {
    service_worker: "scripts/background.bundle.js",
    type: "module",
  },
  action: {
    default_popup: "popup.html",
    default_icon: "public/48x48.png",
  },
  icons: {
    16: "public/16x16.png",
    48: "public/48x48.png",
    128: "public/128x128.png",
  },
  content_scripts: [
    {
      matches: ["<all_urls>"],
      js: ["scripts/content.bundle.js"],
      run_at: "document_idle",
      all_frames: true,
    },
  ],
  web_accessible_resources: [
    {
      resources: ["public/*"],
      matches: ["<all_urls>"],
    },
  ],
  commands: {
    [switchSelectionMode]: {
      suggested_key: {
        default: "Ctrl+Shift+S",
        mac: "MacCtrl+Shift+S",
      },
      description: "Switch selection mode",
    },
  },
  permissions: [
    "tabs",
    "downloads",
    "notifications",
    "storage",
    "unlimitedStorage",
    "webNavigation",
    "history",
    "scripting",
    "contextMenus",
  ],
  host_permissions: ["<all_urls>"],
};

export default manifest;
